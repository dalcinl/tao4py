# Author:  Lisandro Dalcin
# Contact: dalcinl@gmail.com

# --------------------------------------------------------------------

"""
TAO for Python
==============

This package is an interface to TAO_ libraries. TAO is based on PETSc_
and uses MPI_.

.. _TAO:   http://www.mcs.anl.gov/tao
.. _PETSc: http://www.mcs.anl.gov/petsc
.. _MPI:   http://www.mpi-forum.org
"""

# --------------------------------------------------------------------

__author__    = 'Lisandro Dalcin'
__version__   = '2.2'
__credits__   = "TAO Team <tao-comments@mcs.anl.gov>"

# --------------------------------------------------------------------

def init(args=None, arch=None):
    """
    Initializes TAO.

    :Parameters:
      - `args`: command-line arguments, usually the 'sys.argv' list.
      - `arch`: specific configuration to use.

    .. note:: This function should be called only once, typically at
       the very beginning of the bootstrap script of an application.
    """
    import tao4py.lib
    TAO   = tao4py.lib.ImportTAO(arch)
    PETSc = tao4py.lib.ImportPETSc(arch)
    args  = tao4py.lib.getInitArgs(args)
    PETSc._initialize(args)
    TAO._initialize(args)


# --------------------------------------------------------------------

def get_include():
    """
    Return the directory in the package that contains header files.

    Extension modules that need to compile against tao4py should use
    this function to locate the appropriate include directory. Using
    Python distutils (or perhaps NumPy distutils)::

      import petsc4py, tao4py
      Extension('extension_name', ...
                include_dirs=[...,
                              petsc4py.get_include(),
                              tao4py.get_include(),])

    """
    from os.path import dirname, join
    return join(dirname(__file__), 'include')

# --------------------------------------------------------------------

if __name__ == '__main__':
    import tao4py.help
    tao4py.help.help()

# --------------------------------------------------------------------
