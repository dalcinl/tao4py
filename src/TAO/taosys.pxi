cdef extern from "tao.h" nogil:

    enum: TAO_VERSION_MAJOR
    enum: TAO_VERSION_MINOR
    enum: TAO_VERSION_SUBMINOR
    enum: TAO_VERSION_PATCH
    enum: TAO_VERSION_RELEASE
    char* TAO_VERSION_DATE
    char* TAO_AUTHOR_INFO

    int TaoInitializeCalled
    int TaoInitialize(int*,char***,char[],char[])
    int TaoFinalize()

cdef extern from "tao.h" nogil:
    PetscReal TAO_INFINITY
    PetscReal TAO_NINFINITY
    PetscReal TAO_EPSILON
