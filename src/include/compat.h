#ifndef TAO4PY_COMPAT_H
#define TAO4PY_COMPAT_H

#if PETSC_VERSION_(3,2,0)
#define PetscShell PetscFwk
#endif

#if TAO_VERSION_(2,0,0)
#define TAO_VERSION_PATCH TAO_PATCH_LEVEL
#endif

extern PetscBool TaoInitializeCalled;

#endif/*TAO4PY_COMPAT_H*/
