#ifndef TAO4PY_CUSTOM_H
#define TAO4PY_CUSTOM_H

#if TAO_VERSION_(2,0,0)
#include "private/taosolver_impl.h"
#else
#include "tao-private/taosolver_impl.h"
#endif

#undef __FUNCT__
#define __FUNCT__ "TaoGetConstraintTolerances"
PetscErrorCode TaoGetConstraintTolerances(TaoSolver tao, PetscReal *catol, PetscReal *crtol)
{
  PetscFunctionBegin;
  PetscValidHeaderSpecific(tao,TAOSOLVER_CLASSID,1);
  if (catol) *catol=tao->catol;
  if (crtol) *crtol=tao->crtol;
  PetscFunctionReturn(0);
}

#endif/*TAO4PY_CUSTOM_H*/
