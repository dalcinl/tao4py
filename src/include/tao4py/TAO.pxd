# Author:  Lisandro Dalcin
# Contact: dalcinl@gmail.com

# --------------------------------------------------------------------

cdef extern from "tao.h":

    struct _p_TaoSolver
    ctypedef _p_TaoSolver* TaoSolver

# --------------------------------------------------------------------

from petsc4py.PETSc cimport Object

ctypedef public api class Solver(Object) [
    type   PyTaoSolver_Type,
    object PyTaoSolverObject
    ]:
    cdef TaoSolver slv

# --------------------------------------------------------------------
