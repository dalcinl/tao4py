/* Author:  Lisandro Dalcin   */
/* Contact: dalcinl@gmail.com */

#ifndef TAO4PY_H
#define TAO4PY_H

#include <Python.h>

#include <tao.h>

#include "tao4py.TAO_api.h"

static int import_tao4py(void) {
  if (import_tao4py__TAO() < 0) goto bad;
  return 0;
 bad:
  return -1;
}

#endif /* !TAO4PY_H */
