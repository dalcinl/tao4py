/* Author:  Lisandro Dalcin   */
/* Contact: dalcinl@gmail.com */

/* ---------------------------------------------------------------- */

%include petsc4py/petsc4py.i

/* ---------------------------------------------------------------- */

%header %{#include "tao4py/tao4py.h"%}
%init   %{import_tao4py();%}

%define SWIG_TYPECHECK_TAO_SOLVER  650 %enddef

%define %tao4py_objt(Pkg, PyType, Type, CODE, OBJECT_NULL)
%petsc4py_objt(Pkg, PyType, Type, CODE, OBJECT_NULL)
%enddef /* %tao4py_typemap */

/* ---------------------------------------------------------------- */

%tao4py_objt( Tao , Solver , Solver , TAO_SOLVER , PETSC_NULL )

/* ---------------------------------------------------------------- */

/*
 * Local Variables:
 * mode: C
 * End:
 */
