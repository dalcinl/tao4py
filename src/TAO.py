ARCH = None
from tao4py.lib import ImportTAO
from tao4py.lib import ImportPETSc
TAO = ImportTAO(ARCH)
PETSc = ImportPETSc(ARCH)
PETSc._initialize()
TAO._initialize()
del TAO, PETSc
del ImportTAO, ImportPETSc
del ARCH
