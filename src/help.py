# Author:  Lisandro Dalcin
# Contact: dalcinl@gmail.com

"""
Command line access to the TAO Options Database.

This module intends to provide command line access to the TAO
Options Database. It outputs a listing of the many TAO options
indicating option names, default values and descriptions. If you have
Python 2.4 and above, then you can issue at the command line::

  $ python -m tao4py.help [app|solver] [<tao-option-list>]

"""

def help(args=None):
    import sys, tao4py
    tao4py.init(sys.argv + ['-help'])
    from tao4py import TAO
    ARGS = args or sys.argv[1:]
    COMM = TAO.COMM_SELF
    if 'solver' in ARGS or 'tao' in ARGS:
        tao = TAO.Solver().create(comm=COMM)
        tao.setFromOptions()
        tao.destroy()
        del tao

if __name__ == '__main__':
    help()
