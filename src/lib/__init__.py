# Author:  Lisandro Dalcin
# Contact: dalcinl@gmail.com

# --------------------------------------------------------------------

"""
Extension modules for different TAO configurations.

TAO can be configured with different options (eg. debug/optimized,
single/double precisionm, C/C++ compilers, external packages). Each
configuration variant is associated to a name, frequently available as
an environmental variable named ``PETSC_ARCH``.

This package is a holds all the available variants of the TAO
extension module built agaist specific TAO configurations. It also
provides a convenience function using of the builtin ``imp`` module
for easily importing any of the available extension modules depending
on the value of a user-provided configuration name, the ``PETSC_ARCH``
environmental variable, or a configuration file.
"""

# --------------------------------------------------------------------

from petsc4py.lib import ImportPETSc
from petsc4py.lib import Import, getPathArch, getInitArgs


def ImportTAO(arch=None):
    """
    Import the TAO extension module for a given configuration name.
    """
    path, arch = getPathArchTAO(arch)
    PETSc = ImportPETSc(arch)
    return Import('tao4py', 'TAO', path, arch)

def getPathArchTAO(arch=None):
    """
    Undocumented.
    """
    import sys, os
    PETSc = sys.modules.get('petsc4py.PETSc')
    arch = getattr(PETSc, '__arch__', arch)
    path = os.path.dirname(__file__)
    rcvar, rcfile  =  'PETSC_ARCH', 'tao.cfg'
    path, arch = getPathArch(path, arch, rcvar, rcfile)
    return (path, arch)

# --------------------------------------------------------------------
