#!/bin/sh
petsc4py=`python -c "import petsc4py;print(petsc4py.get_include())"`
cython --cleanup 3 -w src -Iinclude -I$petsc4py $@ tao4py.TAO.pyx && \
mv src/tao4py.TAO*.h src/include/tao4py
