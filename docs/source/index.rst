==============
TAO for Python
==============

:Author:       Lisandro Dalcin
:Contact:      dalcinl@gmail.com
:Organization: CIMEC_
:Web Site:     https://bitbucket.org/dalcinl/tao4py
:Date:         |today|

.. include:: abstract.txt

Contents
========

.. include:: toctree.txt

.. include:: links.txt
