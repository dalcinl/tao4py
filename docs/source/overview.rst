Overview
========

*TAO for Python* (tao4py_) is a Python_ package that provides
convenient access to the functionality of TAO_.

TAO [1]_ implements algorithms and tools for the numerical
solution of large, sparse ...


.. [1] Lois Curfman McInnes, Jorge Moré, Todd Munson and Jason Sarich.
   TAO User Manual. ANL/MCS-TM-242 - Revision 1.10.
   Mathematics and Computer Science Division,
   Argonne National Laboratory. 2007

.. include:: links.txt


Features
--------

TAO is intended for use in large-scale application projects ...


Components
----------

TAO components provide the functionality required for the parallel
solutions of large-scale optimization problems.


:TaoSolver: Provides ...

:TaoApp: Provides ...

