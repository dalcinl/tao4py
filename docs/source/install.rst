Installation
============

Requirements
------------

You need to have the following software properly installed in order to
build *TAO for Python*:

* Any MPI_ implementation [#]_ (e.g., MPICH_ or `Open MPI`_), 
  built with shared libraries.

* PETSc_ 3.4 or 3.3 built with shared libraries.

* TAO_ 2.2 or 2.1 built with shared libraries.

* Python_ 2.4 to 2.7 or 3.1 to 3.3 [#]_.

* NumPy_ package.

* petsc4py_ package.

.. [#] Unless you have appropiatelly configured and built TAO and 
       PETSc without MPI (configure option ``--with-mpi=0``).

.. [#] You may need to use a parallelized version of the Python
       interpreter with some MPI-1 implementations (e.g. MPICH1).

.. include:: links.txt


Using **pip** or **easy_install**
---------------------------------

You can use :program:`pip` to install :mod:`tao4py` and its
dependencies (:mod:`mpi4py` is optional but highly recommended)::

  $ pip install [--user] numpy mpi4py
  $ pip install [--user] petsc petsc4py
  $ pip install [--user] tao tao4py

Alternatively, you can use :program:`easy_install` (deprecated)::

  $ easy_install [--user] tao4py

If you already have working PETSc and TAO builds, set environment
variables :envvar:`TAO_DIR` and :envvar:`PETSC_DIR` (and perhaps
:envvar:`PETSC_ARCH` for prefix installs) to appropriate values and
next use :program:`pip`::

  $ export TAO_DIR=/path/to/tao
  $ export PETSC_DIR=/path/to/petsc
  $ export PETSC_ARCH=arch-linux2-c-opt
  $ pip install [--user] petsc4py tao4py


Using **distutils**
-------------------

Downloading
^^^^^^^^^^^

The *TAO for Python* package is available for download at the
project website generously hosted by Bitbucket. You can use
:program:`curl` or :program:`wget` to get a release tarball.

* Using :program:`curl`::

    $ curl -O https://bitbucket.org/dalcinl/tao4py/downloads/tao4py-X.Y.tar.gz

* Using :program:`wget`::

    $ wget https://bitbucket.org/dalcinl/tao4py/downloads/tao4py-X.Y.tar.gz

Building
^^^^^^^^

After unpacking the release tarball::

  $ tar -zxf tao4py-X.Y.tar.gz
  $ cd tao4py-X.Y

the distribution is ready for building.

Some environment configuration is needed to inform the location of
PETSc and TAO. You can set (using :command:`setenv`,
:command:`export` or what applies to you shell or system) the
environment variables :envvar:`TAO_DIR``, :envvar:`PETSC_DIR`, and
:envvar:`PETSC_ARCH` indicating where you have built/installed TAO
and PETSc::

  $ export TAO_DIR=/usr/local/tao
  $ export PETSC_DIR=/usr/local/petsc
  $ export PETSC_ARCH=arch-linux2-c-opt

Alternatively, you can edit the file :file:`setup.cfg` and provide the
required information below the ``[config]`` section::

  [config]
  tao_dir    = /usr/local/tao
  petsc_dir  = /usr/local/petsc
  petsc_arch = arch-linux2-c-opt
  ...

Finally, you can build the distribution by typing::

  $ python setup.py build

Installing
^^^^^^^^^^

After building, the distribution is ready for installation.

If you have root privileges (either by log-in as the root user of by
using :command:`sudo`) and you want to install *TAO for Python* in
your system for all users, just do::

  $ python setup.py install

The previous steps will install the :mod:`tao4py` package at standard
location :file:`{prefix}/lib/python{X}.{X}/site-packages`.

If you do not have root privileges or you want to install *TAO for
Python* for your private use, you have two options depending on the
target Python version.

* For Python 2.6 and up::

    $ python setup.py install --user

* For Python 2.5 and below (assuming your home directory is available
  through the :envvar:`HOME` environment variable)::

    $ python setup.py install --home=$HOME

  Finally, add :file:`$HOME/lib/python` or :file:`$HOME/lib64/python`
  to your :envvar:`PYTHONPATH` environment variable.
