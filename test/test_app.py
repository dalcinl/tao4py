from tao4py import TAO
import unittest

# --------------------------------------------------------------------

class BaseTestApp(object):

    COMM = None

    def setUp(self):
        self.app = TAO.Solver().create(comm=self.COMM)

    def tearDown(self):
        self.app = None

    def testSetRoutinesToNone(self):
        app = self.app
        objective, gradient, objgrad = None, None, None
        constraint, varbounds = None, None
        hessian, jacobian = None, None
        app.setObjective(objective)
        app.setGradient(gradient)
        app.setVariableBounds(varbounds)
        app.setObjectiveGradient(objgrad)
        app.setConstraints(constraint)
        app.setHessian(hessian)
        app.setJacobian(jacobian)

    def testGetVecsAndMats(self):
        app = self.app
        x = app.getSolution()
        g = app.getGradient()
        l, u = app.getVariableBounds()
        r = None#app.getConstraintVec()
        H, HP = None,None#app.getHessianMat()
        J, JP = None,None#app.getJacobianMat()
        for o in [x, g, r, l, u ,H, HP, J, JP,]:
            self.assertFalse(o)

    def testGetKSP(self):
        ksp = self.app.getKSP()
        self.assertFalse(ksp)

# --------------------------------------------------------------------

class TestAppSelf(BaseTestApp, unittest.TestCase):
    COMM = TAO.COMM_SELF

class TestAppWorld(BaseTestApp, unittest.TestCase):
    COMM = TAO.COMM_WORLD

# --------------------------------------------------------------------

if __name__ == '__main__':
    unittest.main()
